import React from "react";
import {render} from "react-dom";

import {Header} from "./components/header/Header";
import {Home} from "./components/home/Home";
import {Button} from "./components/button/Button";

class App extends React.Component {
    constructor(){
        super();
        this.state ={
            homeLink: "Home"
        }
         this.onChangeLinkName = this.onChangeLinkName.bind(this);
    }
    //change the link text
    onChangeLinkName(newName){
        this.setState({
            homeLink : newName
        })
    }
    render(){
        const user = {
            name: "Subhadip",
            age: 24,
            email:"aryan.majumder@gmail.com",
            hobbies:["Sports","Coding"]
        }
        return(
            <div className="container">
                <Header homeLink={this.state.homeLink}/>
                <div className="row">
                    <Home user={user} changeLink={this.onChangeLinkName} initialHomeLink={this.state.homeLink}>
                        <p>This is a paragraph!!</p>
                    </Home>
                </div>
                <hr/>
            </div>
        );
    }
}

render(<App/>, window.document.getElementById("app"));
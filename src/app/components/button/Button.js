import React from "react";

export const Button = (props) => {

    return (
        <button onClick={props.clickEvent} className={props.className}>{props.text}</button>
    );

}

Button.protoTypes ={
    clickEvent: React.PropTypes.func
}
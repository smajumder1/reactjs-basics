import React from "react";
import {Button} from "../../components/button/Button";
import {InputField} from "../../components/inputfield/InputField";

export class Home extends React.Component {
    constructor(props){
        super();
        this.state = {
            age: props.user.age, 
            homeLinkInput : props.initialHomeLink
        }
        this.onMakeOlder = this.onMakeOlder.bind(this);
        this.onChangeLink = this.onChangeLink.bind(this);
    }

    onMakeOlder(){
        this.setState({
            age: this.state.age + 3
        }); 
    };

    //calling parent method to change link text
    onChangeLink(){
        this.props.changeLink(this.state.homeLinkInput) // getting the value from current state
    }

    onChangeHandle(event){
        this.setState({
            homeLinkInput: event.target.value // setting the value from input to current state
        })
    }

    render(){
        return (
            <div className="col-12">
                <p>{this.props.user.name}</p>
                <p>{this.state.age}</p>
                <p>{this.props.user.email}</p>
                <div>
                    <h4>Hobbies</h4>
                    <ul>
                        {this.props.user.hobbies.map((hobby,i) => <li key={i}>{hobby}</li>)}
                    </ul>
                </div>
                <hr/>
                {this.props.children}
                <hr/>
                 {/* <button onClick={() => this.onMakeOlder()} className="btn btn-default">Make me older!!</button>  */}
                 <Button text="Make me older!!" className="btn btn-primary" clickEvent={this.onMakeOlder} />
                 <hr/>
                 <InputField inputType="text" 
                    className="form-control" 
                    placeholderText="Link text" 
                    val={this.state.homeLinkInput}
                    onChange={(event) => this.onChangeHandle(event)}
                />
                 <Button text="Change Home Link" className="btn btn-success" clickEvent={this.onChangeLink} />
            </div>
        );
    }
}

Home.propTypes = {
    user: React.PropTypes.object,
    children: React.PropTypes.element.isRequired
};
import React from "react";

export const InputField = (props) => {

    return (
        <input type={props.inputType} 
            className={props.className} 
            value={props.val} 
            placeholder={props.placeholderText} 
            onChange={props.onChange}
        />
    );

}
